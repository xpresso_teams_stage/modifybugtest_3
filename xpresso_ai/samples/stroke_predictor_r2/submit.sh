#!/bin/bash

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
export SPARK_HOME=/opt/spark/spark-2.4.4-bin-hadoop2.7
export PATH=/opt/spark/spark-2.4.4-bin-hadoop2.7/bin:$PATH



spark-submit --master k8s://https://172.16.3.51:6443 \
            --deploy-mode cluster \
            --name testkoal \
            --driver-cores 1 \
            --executor-cores 1  \
            --driver-memory 512m \
            --executor-memory 512m \
            --conf spark.kubernetes.namespace=default \
            --conf spark.kubernetes.driver.pod.name=testkoal \
            --conf spark.executor.instances=1 \
            --conf spark.kubernetes.container.image=dockerregistry.xpresso.ai/xpr-pyspark-base:u18test2 \
            --conf spark.kubernetes.container.image.pullPolicy=Always \
            --conf spark.kubernetes.container.image.pullSecrets=dockerkey \
            --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark \
            --conf spark.kubernetes.pyspark.pythonVersion=3 \
            Demo_heart_stroke_predictor.py abcd