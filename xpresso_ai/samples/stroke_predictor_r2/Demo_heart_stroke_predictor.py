#!/usr/bin/env python
# coding: utf-8

# In[1]:


from xpresso.ai.core.data.distributed.exploration.distributed_dataset_explorer import Explorer
from xpresso.ai.core.data.distributed.automl.distributed_structured_dataset import DistributedStructuredDataset
from xpresso.ai.core.data.visualization.visualization import Visualization
from xpresso.ai.core.data.connections import Connector
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
import databricks.koalas as ks


# In[2]:


loc_con = {
    "type": "FS",
    "dataset_type": "distributed",
    "delimiter": ",",
    "path": "/xpresso-data/projects/heart_stroke_predictor/input/train/"
}
print(loc_con)


# In[3]:


d_kdf = DistributedStructuredDataset()
d_kdf.import_dataset(user_config=loc_con)
print('Import success!')


# In[4]:


print("Columns:\n",d_kdf.data.columns,'\nShape:\n',d_kdf.data.shape)


# In[5]:


# login here 
get_ipython().run_line_magic('run', '-i /opt/xpresso.ai/scripts/python/xp_login.py qa ashritha.goramane')


# In[6]:


controller_factory = VersionControllerFactory()
request_manager = controller_factory.get_version_controller('hdfs')
print('request manager ready')


# In[7]:


kwargs = {
                "repo_name": "tutorialprojectashritha",
                "branch_name": "demo_distributed",
                "dataset": d_kdf,
                "description": "Heart stroke predictor raw data"
            }
print(kwargs)
response = request_manager.push_dataset(**kwargs)


# In[8]:


print(response)


# In[9]:


d_kdf.data.isna().sum()


# In[10]:


d_kdf.data = d_kdf.data.dropna()


# In[11]:


# Dropped na
d_kdf.data.isna().sum()


# In[12]:


exp = Explorer(d_kdf)
exp.understand()


# In[13]:


exp.explore_univariate(to_excel=True)


# In[14]:


kwargs = {
                "repo_name": "tutorialprojectashritha",
                "branch_name": "demo_distributed",
                "dataset": d_kdf,
                "description": "Cleaned data with xpresso metrics"
            }
print(kwargs)
response_cleaned = request_manager.push_dataset(**kwargs)


# In[15]:


print("response_cleaned_commit", response_cleaned)


# In[16]:


kwargs = {
    "repo_name":"tutorialprojectashritha",
    "branch_name": "demo_distributed",
    "commit_id": response_cleaned[0]
}
new_dataset = request_manager.pull_dataset(**kwargs)


# In[17]:


from xpresso.ai.core.data.exploration.render_exploration import RenderExploration
RenderExploration(new_dataset).render_univariate()


# In[ ]:


from xpresso.ai.core.data.visualization.visualization import Visualization

train_vis = Visualization().get_visualizer(new_dataset)

train_vis.render_univariate(report=True, output_path = "./distributed_report/")


# In[ ]:





# In[ ]:





# In[ ]:


# !jupyter nbconvert --to script Demo_heart_stroke_predictor.ipynb 


# In[ ]:


# !spark-submit  --name Koalas --master spark://172.16.2.10:7077 --num-executors=7 --driver-memory 2G --executor-memory 2G Demo_heart_stroke_predictor.py 

